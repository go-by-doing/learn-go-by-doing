module learn-go-by-doing

go 1.22.5

require github.com/otiai10/gosseract/v2 v2.4.1

require golang.org/x/exp v0.0.0-20240909161429-701f63a606c0 // indirect
