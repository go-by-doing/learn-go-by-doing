package main

import "fmt"
//import "strings"

func main() {

	//var a [6]string = [6]string{"ב", "ה", "צ", "ל", "ח", "ה"}
	//var b []string = []string{"h", "e", "l", "l", "o"}
	var c []string = []string{"ב", "ה", "צ", "ל", "ח", "ה"}
	//var d []string = []string{"ב", "ה", "צ", "ל", "ח", "ה"}

	//answera := RecursionArrayToString(d, len(d))
	//answerb := RecursionArrayToStringV2(c, 0)
	//answerc := RecursionSliceToString_MT(c, "")
	answerc := NonRecursion(c)

	//fmt.Printf("RecursionArrayToString: %s\n", answera)
	//fmt.Printf("RecursionArrayToStringV2: %s\n", answerb)
	//fmt.Printf("RecursionArrayToString_MT: %s\n", answerc)
	fmt.Printf("NonRecursion: %s\n", answerc)
}

func trace(s string) {fmt.Println("entering:", s)}
func untrace(s string) {fmt.Println("leaving:", s)}

// MT: Don't use arrays, use slices ([5]string -> []string)
func RecursionArrayToStringV2(str []string, index int) string {
	fmt.Println("I'm executing RecursionArrayToStringV2")

	// Print the length before the increment
	fmt.Printf("index before increment: %d\n", index)

	if index == len(str) {
		return ""

	}

	return str[index] + RecursionArrayToStringV2(str, index+1)

}

func RecursionArrayToString(str []string, length int) string {
	fmt.Println("I'm executing RecursionArrayToString")

	// Print the length before the decrement
	fmt.Printf("index before decrement: %d\n", length)

	if length == 0 {

		return ""
	}

	fmt.Println("Length of an Array is :", length)

	return str[length-1] + RecursionArrayToString(str, length-1)

}

func NonRecursion(str []string) (result string) {
	fmt.Println("I'm executing NonRecursion")

	//calculate size of the array a
	strLength := len(str)

	//display the size
	println("Length of an Array is :", strLength)
	// MT: Shouldn't you return the string?
	// Print the indices and elements.
	for _, v := range str {

		result = v + result
		
	}
	return result
}

// MT: Another version for RecursionSliceToString
func RecursionSliceToString_MT(str []string, s string) string {
	if len(str) == 0 {

		return s
	}

	fmt.Println("s: ", s, "length str:", len(str), "str[1:] ", str[1:], "s+str[0]: ", s+str[0])
	return RecursionSliceToString_MT(str[1:], s+str[0])
}


func RecursionSliceToStringReverse(str []string, s string) string {
	if len(str) == 0 {

		return s
	}

	fmt.Println("s: ", s, "length str:", len(str), "str[:len(str)-1]: ", str[:len(str)-1], "s+str[len(str)-1]: ", s+str[len(str)-1])
	return RecursionSliceToStringReverse(str[:len(str)-1], s+str[len(str)-1])
}


func RecursiveNumber(number int) int {
	fmt.Println("I'm executing RecursiveNumber")
	if number == 1 {

		return number
	}

	return number + RecursiveNumber(number-1)
}
