package main

import (
	"testing"
)

func TestCleanText(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected string
	}{
		{
			name:     "Remove special characters",
			input:    "Hello\n***\nWorld",
			expected: "Hello\nWorld",
		},
		{
			name:     "Remove unreadable content",
			input:    "Valid text\nR?\nMore valid text",
			expected: "Valid text\nMore valid text",
		},
		{
			name:     "Remove isolated numbers",
			input:    "Item 1: 100\n2002\nItem 2: 200",
			expected: "Item 1: 100\nItem 2: 200",
		},
		{
			name:     "Trim spaces",
			input:    "  Trimmed   line  \n  Another   line  ",
			expected: "Trimmed line\nAnother line",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := CleanText(tt.input)
			if result != tt.expected {
				t.Errorf("CleanText() = %v, want %v", result, tt.expected)
			}
		})
	}
}