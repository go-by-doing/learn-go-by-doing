 package main

// import (
// 	"encoding/json"
// 	"fmt"
// 	"io"
// 	"os"
// 	"regexp"
// 	"strings"
// )

// // Response represents the structure of the Google Vision API response
// type Response struct {
// 	TextAnnotations []struct {
// 		Description string `json:"description"`
// 	} `json:"textAnnotations"`
// }

// var (
// 	specialCharRegex     = regexp.MustCompile(`(?m)^[^\w\s\p{Hebrew}]+$`)
// 	unreadableRegex      = regexp.MustCompile(`(?m)^([A-Za-z](\?)?|[^\w\s\p{Hebrew}]{1,2})$`)
// 	isolatedNumberRegex  = regexp.MustCompile(`(?m)^(\d{1,6})$`)
// 	emptyLineRegex       = regexp.MustCompile(`(?m)^\s*$`)
// 	extraSpaceRegex      = regexp.MustCompile(`\s{2,}`)
// 	hebrewRegex          = regexp.MustCompile(`\p{Hebrew}`)
// 	noiseRegex           = regexp.MustCompile(`(?i)(\.X\.)`)
// )

// func CleanText(text string) string {
// 	lines := strings.Split(text, "\n")
// 	var cleanedLines []string

// 	for _, line := range lines {
// 		line = strings.TrimSpace(line)
		
// 		// Skip empty lines and lines with only special characters
// 		if emptyLineRegex.MatchString(line) || specialCharRegex.MatchString(line) {
// 			continue
// 		}

// 		// Remove unreadable content and isolated numbers (except for the total amount)
// 		if unreadableRegex.MatchString(line) || (isolatedNumberRegex.MatchString(line) && line != "142.37") {
// 			continue
// 		}

// 		// Remove noise like ".X."
// 		line = noiseRegex.ReplaceAllString(line, "")

// 		// Keep lines with Hebrew text or longer than 3 characters
// 		if len(line) > 3 || hebrewRegex.MatchString(line) {
// 			cleanedLines = append(cleanedLines, line)
// 		}
// 	}

// 	cleanedText := strings.Join(cleanedLines, "\n")
// 	return extraSpaceRegex.ReplaceAllString(cleanedText, " ")
// }

// // ReadJSONFile reads and returns the contents of a JSON file
// func ReadJSONFile(filename string) ([]byte, error) {
// 	file, err := os.Open(filename)
// 	if err != nil {
// 		return nil, fmt.Errorf("error opening file: %w", err)
// 	}
// 	defer file.Close()

// 	return io.ReadAll(file)
// }

// // ExtractAndCleanText parses JSON data and cleans the extracted text
// func ExtractAndCleanText(jsonData []byte) (string, error) {
// 	var response Response
// 	if err := json.Unmarshal(jsonData, &response); err != nil {
// 		return "", fmt.Errorf("error parsing JSON: %w", err)
// 	}

// 	if len(response.TextAnnotations) == 0 {
// 		return "", fmt.Errorf("no text annotations found in the JSON")
// 	}

// 	return CleanText(response.TextAnnotations[0].Description), nil
// }

// // ProcessJSONFile reads a JSON file, extracts text, and cleans it
// func ProcessJSONFile(filename string) (string, error) {
// 	jsonData, err := ReadJSONFile(filename)
// 	if err != nil {
// 		return "", fmt.Errorf("error reading file: %w", err)
// 	}

// 	return ExtractAndCleanText(jsonData)
// }
