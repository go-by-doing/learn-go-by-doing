package main

// import (
// 	"encoding/json"
// 	"fmt"
// 	"os"
// 	"regexp"
// 	"strings"
// )

// // Response represents the structure of the Google Vision API response
// type Response struct {
// 	TextAnnotations []struct {
// 		Description string `json:"description"`
// 	} `json:"textAnnotations"`
// }

// func cleanText(text string) string {
// 	// Remove lines consisting only of asterisks
// 	asteriskRegex := regexp.MustCompile(`(?m)^\*+$`)
// 	text = asteriskRegex.ReplaceAllString(text, "")

// 	// Remove lines with unreadable content (e.g., "R?")
// 	unreadableRegex := regexp.MustCompile(`(?m)^.*\?.*$`)
// 	text = unreadableRegex.ReplaceAllString(text, "")

// 	// Remove empty lines
// 	emptyLineRegex := regexp.MustCompile(`(?m)^\s*$`)
// 	text = emptyLineRegex.ReplaceAllString(text, "")

// 	// Trim spaces from each line
// 	var cleanedLines []string
// 	for _, line := range strings.Split(text, "\n") {
// 		trimmedLine := strings.TrimSpace(line)
// 		if trimmedLine != "" {
// 			cleanedLines = append(cleanedLines, trimmedLine)
// 		}
// 	}

// 	return strings.Join(cleanedLines, "\n")
// }

// func main() {
// 	// Read the JSON file
// 	jsonData, err := os.ReadFile("YovavReceipt.json")
// 	if err != nil {
// 		fmt.Printf("Error reading file: %v\n", err)
// 		return
// 	}

// 	// Parse the JSON
// 	var response Response
// 	err = json.Unmarshal(jsonData, &response)
// 	if err != nil {
// 		fmt.Printf("Error parsing JSON: %v\n", err)
// 		return
// 	}

// 	// Extract and clean the text
// 	if len(response.TextAnnotations) > 0 {
// 		extractedText := response.TextAnnotations[0].Description
// 		cleanedText := cleanText(extractedText)
// 		fmt.Println("Cleaned extracted text:")
// 		fmt.Println(cleanedText)
// 	} else {
// 		fmt.Println("No text annotations found in the JSON.")
// 	}
// }