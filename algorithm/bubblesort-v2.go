package main

import (
	"fmt"
	"bufio"
	"os"
	"unicode"
	"strconv"
)

func bubbleSort (table []int) {
	var i, j int
	for i = 0; i < len(table); i++ {
		for j = len(table) - 1 ; j > i; j-- {
			if table[j - 1] > table[j] {
				swap(table, j)
			}
		}
	}
}

func swap (table []int, j int) {
	var temp int
	temp = table[j]
	table[j] = table[j-1]
	table[j-1] = temp
}

func IsAlpha(s string) bool {
    for _, r := range s {
        if !unicode.IsLetter(r) {
            return false
        }
    }
    return true
}

func main () {
	
	var tab []int
	var in int
	input := bufio.NewScanner(os.Stdin)
	fmt.Printf("Insert up to 10 values for the array, type a char to end:\n");
	for input.Scan() {
		if IsAlpha(input.Text()) || len(tab) == 10 {
			break
		} else {
			in, _ = strconv.Atoi(input.Text())
			tab = append(tab, in)
		}
	}
	fmt.Printf("Sorted successfully.\n")
	bubbleSort(tab)
	for _, item := range tab {
		fmt.Printf("%d\t", item)
	}
}


