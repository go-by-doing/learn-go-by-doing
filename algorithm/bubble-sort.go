// Bubble sort exercise
// Sharon Mafgaoker. 27.07.2023
package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)
// The program should print the integers out on one line 
// in sorted order, from least to greatest.
func main() {

	//Display what you want from user
	fmt.Println("Type in a sequence of up to 10 integers to be sorted (space separated): ")

	//Take input from user
	input := bufio.NewReader(os.Stdin)
	data, err := input.ReadString('\n')
	if err != nil {
		// MT: Don't panic, use log.Fatalf("error: %s", err)
		// MT: Users see a stack trace and they think it's a bug
		log.Fatalf("error: %s", err)
	}
	// MT: Use strings.Fields (no need to trim then)
	dataSplit := strings.Fields(data)
	if len(dataSplit) > 10{
		log.Fatal("Found more then 10 digits, Enter 10 digits max!")
	}


	numbers := make([]int, len(dataSplit))
	for i, v := range dataSplit {
		// MT: Naming. This should be called "number" (or "n")
		number, err := strconv.Atoi(v)
		if err != nil {
			log.Fatalf("can't convert string to Int: %v", err)
		}
		numbers[i] = number 
	}

	BubbleSort(numbers)

	for _, n := range numbers{
		fmt.Printf("%d ", n)
	}
	fmt.Println()
	
}

// BubbleSort algorithm 
// Sorting from least to greatest a slice of ints in place.
// MT: Document that you're changing the slice in place (vs returning new sorted slice)
func BubbleSortv1(integers []int) {
	log.Println("I'm excuting BubbleSort (slice of ints in place)")
	for l := len(integers); l > 0; l-- {
		for i := 0; i < l-1; i++ {
			if integers[i] > integers[i+1] {
				//Swap(integers, i)
				integers[i], integers[i+1] = integers[i+1], integers[i] 
			}
		}
	}
}

// MT: IMO this function is not required. You can do the following inside the "if"
// MT: integers[i], integers[i+1] = integers[i+1], integers[i]
func Swapv1(integers []int, index int) {
	log.Println("I'm excuting Swap")
	big, small := integers[index], integers[index+1]
	integers[index] = small
	integers[index+1] = big
}
