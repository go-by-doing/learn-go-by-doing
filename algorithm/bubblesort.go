// Bubble sort exercise
// Richard Velasquez. 22.07.2023

package main

import (
	"fmt"
)

func main() {
	var number int
	numSlice := make([]int, 0, 10)
	//Capture 10 integers inside a slice
	fmt.Println("Please input ten(10) integers:")
	for i := 0; i < 10; i++ {
		fmt.Scanln(&number)
		numSlice = append(numSlice, number)
	}
	BubbleSort(numSlice)
	fmt.Println("The sorted slice is", numSlice)
}

// Bubble sort algorithm
func BubbleSort(slice []int) {
	for k := 0; k < len(slice)-1; k += 1 {
		for j := 0; j < len(slice)-1; j += 1 {
			if slice[j+1] < slice[j] {
				Swap(slice, j)
			}
		}
	}
}

// Swap 2 elements on the slice depending on index
func Swap(slice []int, index int) {
	tmpe := slice[index]
	slice[index] = slice[index+1]
	slice[index+1] = tmpe
}
