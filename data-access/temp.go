package main

import (
	"fmt"
	"log"
	"time"

	"github.com/goburrow/modbus"
)

func main() {
	// Define the serial port configuration
	// serialConfig := &serial.Config{
	// 	Name:        "/dev/ttyUSB0", // Replace with the actual serial port name
	// 	Baud:        9600,           // Adjust the baud rate as needed
	// 	ReadTimeout: 1 * time.Second,
	// }

	// // Open the serial port for communication
	// port, err := serial.OpenPort(serialConfig)
	// if err != nil {
	// 	log.Fatalf("Failed to open serial port: %v", err)
	// }
	// defer port.Close()

	// Modbus RTU/ASCII
	handler := modbus.NewASCIIClientHandler("/dev/tty.usbserial-0001")
	handler.DataBits = 8
	handler.SlaveId = 1 // Set the slave ID to match your PLC's address
	handler.Parity = "E"
	handler.Timeout = 5 * time.Second

	err := handler.Connect()
	if err != nil {
		log.Fatalf("Failed to handler.Connect: %v", err)
	}
	defer handler.Close()
	client := modbus.NewClient(handler)

	// Define the Modbus address for the register you want to read
	registerAddress := uint16(0) // Replace with the actual register address

	// Read a holding register from the PLC
	results, err := client.ReadDiscreteInputs(registerAddress, 1)
	if err != nil {
		log.Fatalf("Failed to read holding register: %v", err)
	}

	// Print the result
	fmt.Printf("Holding Register Value: %v\n", results[0])
}
