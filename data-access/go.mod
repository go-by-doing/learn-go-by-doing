module example/data-access

go 1.21.0

require github.com/go-sql-driver/mysql v1.7.1

require (
	github.com/goburrow/modbus v0.1.0 // indirect
	github.com/goburrow/serial v0.1.0 // indirect
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07 // indirect
	github.com/tbrandon/mbserver v0.0.0-20211210035124-daf3c8c4269f // indirect
	golang.org/x/sys v0.12.0 // indirect
)
