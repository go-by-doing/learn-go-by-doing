package main

import (
	"fmt"
	"log"
	"time"

	"github.com/goburrow/modbus"
)

func main() {
	// Define the serial port configuration
	// serialConfig := &serial.Config{
	// 	Name:        "/dev/tty.usbserial-0001", // Replace with the actual serial port name
	// 	Baud:        9600,                      // Adjust the baud rate as needed
	// 	ReadTimeout: 5 * time.Second,
	// }

	// // Open the serial port for Modbus RTU communication
	// port, err := serial.OpenPort(serialConfig)
	// if err != nil {
	// 	log.Fatalf("Failed to open serial port: %v", err)
	// }
	// defer port.Close()

	// Create a Modbus RTU client
	handler := modbus.NewASCIIClientHandler("/dev/tty.usbserial-0001")
	handler.BaudRate = 9600 // Adjust the baud rate to match your PLC
	handler.DataBits = 7    // 8 data bits
	handler.Parity = "E"    // No parity
	handler.StopBits = 1    // 1 stop bit
	handler.SlaveId = 1
	handler.Timeout = 5 * time.Second

	err := handler.Connect()
	if err != nil {
		log.Fatalf("Failed to handler.Connect: %v", err)
	}
	defer handler.Close()

	client := modbus.NewClient(handler)
	// Define the Modbus address for the register you want to read
	//registerAddress := uint16(0) // Replace with the actual register address

	results, err := client.ReadInputRegisters(uint16(1), uint16(100))
	if err != nil {
		log.Fatalf("Failed to read holding register: %v", err)
	}
	// Print the result
	fmt.Printf("Holding Register Value: %v\n", results)
}
