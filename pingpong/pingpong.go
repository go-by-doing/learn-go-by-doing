/*
Implement a ping pong game. Each player represented by a goroutine.
The game will continue for a duration of 2 minutes.
Players will take turns holding the ball, waiting for a random time (0–10 seconds), and then serving it back.
Each player serves at random intervals, adding an element of unpredictability to the game.
At the end of the 2-minute period, the player holding the ball is declared the winner.
*/

package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const gameDuration = 10 * time.Second

// Player represents a ping pong player
type Player struct {
	name string
}

// Serve simulates the player serving the ball back
func (p *Player) Serve(ball chan *Player, wg *sync.WaitGroup) {
	defer wg.Done()

	// Start timer for the game duration
	gameTimer := time.After(gameDuration)
	
	for {
		select {
		case ball <- p:
			fmt.Println("sends\n", ball)
			// Random delay between 0 and 10 seconds
			time.Sleep(time.Duration(rand.Intn(5)) * time.Second)
		case <-gameTimer:
			fmt.Printf("%s quit serving\n", p.name)
			return
		default:
			fmt.Printf("serving..")
		}
	}
}

func main() {

	// Create channels and wait group
	ball := make(chan *Player)
	var wg sync.WaitGroup

	// Initialize players
	player1 := &Player{name: "Player 1"}
	player2 := &Player{name: "Player 2"}

	// Start the game
	wg.Add(2)
	go player1.Serve(ball, &wg)
	go player2.Serve(ball, &wg)

	// Start timer for the game duration
	gameTimer := time.After(gameDuration)
	var winner *Player

	// Play the game
	for {
		select {
		case current := <-ball:
			winner = current
			fmt.Printf("%s has the ball\n", current.name)
		case <-gameTimer:
			// Game over, declare the winner
			fmt.Printf("Game over! %s is the winner!\n", winner.name) 
			wg.Wait() 
			close(ball)
			return
		}
	}
}
