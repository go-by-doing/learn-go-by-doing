/*
Implement a ping pong game. Each player represented by a goroutine.
The game will continue for a duration of 2 minutes.
Players will take turns holding the ball, waiting for a random time (0–10 seconds), and then serving it back.
Each player serves at random intervals, adding an element of unpredictability to the game.
At the end of the 2-minute period, the player holding the ball is declared the winner.
*/

package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const gameDuration = 20 * time.Second

// Player represents a ping pong player
type Player struct {
	name string
}

// Serve simulates the player serving the ball back
func (p *Player) Serve(ctx context.Context, ball chan *Player, wg *sync.WaitGroup) {
	defer wg.Done()
	defer fmt.Printf("%s quit serving\n", p.name)

	// Start timer for the game duration
	// gameTimer := time.After(gameDuration)

	for {
		select {
		case <-ctx.Done():
			return
		case ball <- p:
			fmt.Printf("%s serv the ball\n", p.name)
			// Random delay between 0 and 10 seconds
			time.Sleep(time.Duration(rand.Intn(10)) * time.Second)
		// case <-gameTimer:
		// 	fmt.Printf("%s quit serving\n", p.name)
		// 	return
			// default: // A select may have a default, which specifies what to do when none of the other communications can proceed immediately.
			// 	fmt.Printf("%s wait to be serv\n",p.name) // For debug the flow...
		}
	}
	
}

func main() {
	// defines a timeout context that will be canceled after the game Duration, use to signal the player to stop serving ball.
	ctxTimeout, cancel := context.WithTimeout(context.Background(), gameDuration)
	defer cancel()

	// Create channels and wait group
	ball := make(chan *Player)
	var wg sync.WaitGroup

	// Initialize players
	player1 := &Player{name: "Player 1"}
	player2 := &Player{name: "Player 2"}

	// Start the game
	wg.Add(2)
	go player1.Serve(ctxTimeout,ball, &wg)
	go player2.Serve(ctxTimeout,ball, &wg)

	// Start timer for the game duration
	//gameTimer := time.After(gameDuration)
	var winner *Player

	// closer
	go func() {
		wg.Wait()
		close(ball)
	}()

	// Play the game
	for player := range ball {
		winner = player
	}

	// Game over, declare the winner
	fmt.Printf("Game over! %s is the winner!\n", winner.name)

}
