package main

// A unit of work
type Work struct {
	x, y, z int
}

// A worker Task
func worker(in <-chan *Work, out chan<- *Work) {
	for w := range in {
		w.z = w.x * w.y
		Sleep(w.z)
		out <- w
	}
	//Must make sure other workers can run when one blocks.
}

func main() {

}
