#!/usr/bin/env sh
# prerequsite
#https://github.com/BurntSushi/ripgrep#installation

# MT: Why not use the Go test suite?

set -e

echo "=== Test GET ==="
# MT: Use common tools (rg -> grep)
curl -X GET -s localhost:8080 | rg -q "GET" 

echo "=== Test POST ==="
curl -X POST -s localhost:8080 | rg -q "POST"

echo "Success"

