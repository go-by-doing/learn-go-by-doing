package main

import (
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

// MT: The db file shouldn't be in git
const dbfile string = "randomnumbers.db?_journal=WAL&_timeout=5000&_fk=true"

// - Set WAL mode (not strictly necessary each time because it's persisted in the database, but good for first run)
// - Set busy timeout, so concurrent writers wait on each other instead of erroring immediately
// - Enable foreign key checks

func main() {
	// Handle sigterm and await termChan signal
	sigChannel := make(chan os.Signal, 1)
	signal.Notify(sigChannel, os.Interrupt, syscall.SIGTERM)

	db, err := sql.Open("sqlite3", dbfile)
	if err != nil {
		log.Panic("Failed to open database:", err)
	}

	go func() {
		<-sigChannel
		fmt.Println("Program killed!")
		db.Exec("PRAGMA wal_checkpoint(TRUNCATE)")
		db.Close() // MT: Don't ignore errors
		log.Println("Database closed")
		os.Exit(1)
	}()

	// MT: Create utility function that does the pragma and close and use it here and in the above defer
	defer func() {
		db.Exec("PRAGMA wal_checkpoint(TRUNCATE)")
		_ = db.Close() // MT: Don't ignore errors
		log.Println("Database closed")
	}()

	// MT: if err := db.Ping(); err != nil { ...
	// MT: This way you won't need to find new name for the error
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected!")

	// CreateTable create two tables divideby2 & divideby3
	err = CreateTable(db)
	if err != nil {
		log.Fatal("Failed to create table:", err)
	}

	http.HandleFunc("/", HelloServer)
	http.HandleFunc("/printhello", PrintHello)
	http.HandleFunc("/logicbasiceven", LogicBasicEven)
	http.HandleFunc("/logicbasicdivideby3", LogicBasicDivideBy3)
	http.HandleFunc("/logicrecursioneven", LogicRecursionDivideBy2(db))
	http.HandleFunc("/logicrecursiondivideby3", LogicRecursionDivideBy3(db))

	log.Println("Starting listening on port 8080")
	log.Fatal(http.ListenAndServe(":8080", nil))

}
// Trace function
// Args evaluate now, defer later
func trace(s string) string {
 	fmt.Println("entering:", s)
	return s
}
func un(s string) {
	fmt.Println("leaving:", s)
}

// Database
func CreateTable(db *sql.DB) error {
	defer un(trace("CreateTable"))
	fmt.Println("in CreateTable")
	//log.Println("I am excuting CreateTable")
	// MT: Write SQL in .sql files and use the embed package
	sqlStmt := `
		CREATE TABLE IF NOT EXISTS divideby3 (
			id INTEGER PRIMARY KEY NOT NULL,
			timestamp DATETIME NOT NULL,
			number INTEGER NOT NULL,
			attempts INTEGER NOT NULL);
			
		CREATE TABLE IF NOT EXISTS divideby2 (
			id INTEGER PRIMARY KEY NOT NULL,
			timestamp DATETIME NOT NULL,
			number INTEGER NOT NULL,
			attempts INTEGER NOT NULL);`
	// MT: if _, err := db.Exec(sqlStmt); err != nil { ...
	if _, err := db.Exec(sqlStmt); err != nil {
		log.Fatal("Failed to create table:", err)
	}
	return nil
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
	log.Println("I am excuting HelloServer")
	fmt.Fprintf(w, "Hello you! the http method i can see: %s, %s!", r.Method, r.URL.Path[1:])
}

func LogicRecursionDivideBy3(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("I am excuting LogicRecursionDivideBy3")
		randomNum, attempts := randomDivideBy3Recursion(0)
		err := InsertIntoDivby3(db, randomNum, attempts)
		if err != nil {
			log.Fatal("Failed to exec InsertIntoDivby3: ", err)
		}
		fmt.Fprintf(w, "My random divide by 3 number is %d", randomNum)
	}

}

// MT: This is the same as LogicRecursionDivideBy3 (only calling randomDivideBy2Recursion. Use a middleware.
func LogicRecursionDivideBy2(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("I am excuting LogicRecursionDivideBy2")
		randomNum, attempts := randomDivideBy2Recursion(0)
		err := InsertIntoDivby2(db, randomNum, attempts)
		if err != nil {
			log.Fatal("Failed to exec InsertIntoDivby2: ", err)
		}
		fmt.Fprintf(w, "My random even number is %d", randomNum)
	}
}
func PrintHello(w http.ResponseWriter, r *http.Request) {
	log.Println("I am excuting PrintHello")
	randomNum := random()
	log.Println("My random number is", randomNum)
	if isEven(randomNum) {
		log.Println("My random number is Even")
	} else {
		log.Println("My random number is Odd")
	}

	fmt.Fprintf(w, "Hello World, %d", randomNum)

}

func LogicBasicEven(w http.ResponseWriter, r *http.Request) {
	log.Println("I am excuting LogicBasicEven")
	randomEven := randomEven()
	fmt.Fprintf(w, "My random even number is %d", randomEven)

}

func LogicBasicDivideBy3(w http.ResponseWriter, r *http.Request) {
	log.Println("I am excuting LogicbasicDivideBy3")
	randomNumDivideBy3 := randomDivideBy3()
	println(randomNumDivideBy3)
	fmt.Fprintf(w, "My random divide by 3 number is %d", randomNumDivideBy3)

}

// Returns a random number. It's actually deterministic here because
// we don't seed the RNG, but it's an example of a non-pure function
// from SQLite's POV.
// Int63 returns a non-negative pseudo-random 63-bit
// integer as an int64 from the default Source.
// func getrand() int64 {
// 	log.Println("I am excuting getrandom")
// 	return rand.Int63()
// }

// MT: This function is just noise
func random() int {
	log.Println("I am excuting random")
	return rand.Int()
}

func isEven(number int) bool {
	// MT: Don't log in low level library functions
	log.Println("I am excuting isEven")
	return number%2 == 0
}

func isDivideBy3(number int) bool {
	log.Println("I am excuting isDivideBy3")
	return number%3 == 0
}

func randomEven() int {
	log.Println("I am excuting randomEven")

	randomNum := random()
	log.Println("My randomNum is ", randomNum)
	// MT: You can do (randomNum / 2) * 2
	return (randomNum / 2) * 2

}

func randomDivideBy3() int {
	log.Println("I am excuting randomDivideBy3")
	randomNum := random()
	log.Println("My randomNum is ", randomNum)
	if isDivideBy3(randomNum) {
		return randomNum
	} else if randomNum%3 == 1 {
		return randomNum + 2
	} else {
		return randomNum + 1
	}
}

// func randomEvenRecursion(index int) int {
// 	log.Println("I am excuting randomEvenRecursion in the index:", index)

// 	randomNum := random()
// 	log.Println("My randomNum is ", randomNum)
// 	if isEven(randomNum) {
// 		return randomNum
// 	} else {
// 		return randomEvenRecursion(index + 1)
// 	}

// }

func randomDivideBy2Recursion(index int) (number int, attempts int) {
	log.Println("I am excuting randomDivideBy2Recursion in the index:", index)

	randomNum := random()
	log.Println("My randomNum is ", randomNum)
	if isEven(randomNum) {
		return randomNum, index
	} else {
		return randomDivideBy2Recursion(index + 1)
	}

}

// MT: You can unite this and randomDivideBy2Recursion.
func randomDivideBy3Recursion(index int) (number int, attempts int) {
	log.Println("I am excuting randomDivideBy3Recursion in the index:", index)

	randomNum := random()
	log.Println("My randomNum is ", randomNum)
	if isDivideBy3(randomNum) {
		return randomNum, index
	} else {
		return randomDivideBy3Recursion(index + 1)
	}
}

func InsertIntoDivby3(db *sql.DB, number int, attempts int) error {
	log.Println("I am excuting InsertIntoDiveby3")
	_, err := db.Exec("INSERT INTO divideby3 VALUES(NULL,?,?,?);", time.Now(), number, attempts)
	if err != nil {
		log.Fatal("Failed to insert records:", err)
	}
	return nil
}

func InsertIntoDivby2(db *sql.DB, number int, attempts int) error {
	log.Println("I am excuting InsertIntoDiveby2")
	// MT: Why do you insert NULL to id that has a NOT NULL constraint. You can specify only the columns you insert into.
	// MT: See https://www.w3schools.com/sql/sql_insert.asp
	_, err := db.Exec("INSERT INTO divideby2 VALUES(NULL,?,?,?);", time.Now(), number, attempts)
	if err != nil {
		log.Fatal("Failed to insert records:", err)
	}
	return nil
}

//TODO
//1. create database randomnumbers
//2. create table divideby2 with column (id, timestemp, number, attempts)
//3. create table divideby3 ""
