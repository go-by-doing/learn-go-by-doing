package main

import "fmt"

//exercises
/*
You all know what the Fibonacci series is.
Write a package to implement it. There
should be a function to get the next value.
(You don't have structs yet; can you find a
way to save state without globals?) But
instead of addition, make the operation
settable by a function provided by the user.
Integers? Floats? Strings? Up to you.
Write a gotest test for your package.
*/

func main(){
	fmt.Println("test")
}


