package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
)

func main() {

	url := "https://api.ai21.com/studio/v1/improvements"

	payload := strings.NewReader("{\"types\":[\"fluency\",\"vocabulary/specificity\",\"vocabulary/variety\",\"clarity/short-sentences\",\"clarity/conciseness\"],\"text\":\"Dedicated to optimization & Proud Geek. I am an experienced DevOps Engineer who understands the Software Development Lifecycle and brings software engineering tools and processes to solve classic operations challenges.  I have Cloud and monitoring experience, as well as being a DevOps developer for Windows and Linux systems. I am an AWS Certified solution architect and have been working as a DevOps Engineer since shortly after the concept was introduced.  I have a strong fundamental understanding of how computer and electronic technologies work, and I am a good self-teacher and effective researcher and troubleshooter. So when I encounter a new system or problem, normally I can quickly understand it well enough to get started.\"}")
	fmt.Println()
	req, err := http.NewRequest("POST", url, payload)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Add("accept", "application/json")
	req.Header.Add("content-type", "application/json")
	req.Header.Add("Authorization", "Bearer iNvXJrAxvCdBHfWCXAhWE9Wj7AfMpsWs")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)

	fmt.Println("response Body:", string(body))

}
